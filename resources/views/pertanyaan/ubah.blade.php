@extends('adminlte.master')

@section('content')

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Ubah Pertanyaan Anda</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <form role="form" action="/pertanyaan/{{ $tanya->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
          <div class="col-sm-6">
            <!-- text input -->
            <div class="form-group">
              <label>Judul</label>
              <input type="text" id="judul" name="judul" class="form-control" value="{{ old('judul', $tanya->judul) }}" placeholder="" required>
                @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <!-- textarea -->
            <div class="form-group">
              <label>Isi</label>
              <input type="text-area" class="form-control" id="isi" name="isi" rows="5" value="{{ old('isi', $tanya->isi) }}" placeholder="" required>
              @error('isi')
                  <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
    <!-- /.card-body -->
  </div>

@endsection
