@extends('adminlte.master')

@push('style')
<link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('content')

    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Tabel Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">

            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif

            <a class="btn btn-primary mb-3" href="/pertanyaan/create">Ajukan Pertanyaan</a>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th class="text-center">Judul</th>
                <th class="text-center">Isi</th>
                <th class="text-center" style="width: 40px">Aksi</th>
              </tr>
            </thead>
            <tbody>

            @forelse ($pertanyaan as $key => $tanya)
                <tr>
                    <td>{{ $key +1 }}</td>
                    <td>{{ $tanya -> judul }}</td>
                    <td>{{ $tanya -> isi }}</td>
                    <td style="display: flex;">
                        <a href="/pertanyaan/{{ $tanya->id }}" class="btn btn-info btn-sm mr-2">Lihat</a>
                        <a href="/pertanyaan/{{ $tanya->id }}/ubah" class="btn btn-warning btn-sm mr-2">Ubah</a>
                        <form action="/pertanyaan/{{ $tanya->id }}" method="post">
                            @csrf
                            @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger">
                        </form>
                    </td>
                </tr>
                @empty
                    <tr>
                        <td colspan="4" align="center">Tidak Ada Pertanyaan!</td>
                    </tr>
                @endforelse

            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        {{-- <div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
          </ul>
        </div> --}}
      </div>

@endsection
@push('script')
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush
