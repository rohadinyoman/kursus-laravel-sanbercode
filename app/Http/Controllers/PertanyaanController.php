<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Tanya;

class PertanyaanController extends Controller
{
    public function index()
    {
        // $pertanyaan = DB::table('pertanyaan')->get();   //Select * From pertanyaan
        $pertanyaan = Tanya::all();
        // dd($pertanyaan);
        return view('/pertanyaan.index', compact('pertanyaan'));
    }
    public function create()
    {
        return view('/pertanyaan.create');
    }
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'judul' => "required|unique:pertanyaan",
            'isi' => 'required'
        ]);

        // $query = DB::table('pertanyaan')->insert([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"]
        // ]);

        // $tanya = new Tanya;
        // $tanya->judul = $request['judul'];
        // $tanya->isi = $request['isi'];
        // $tanya->save();

        $tanya = Tanya::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan disimpan!');
    }
    public function show($id)
    {
        // $tanya = DB::table('pertanyaan')->where('id', $id)->first();
        // dd($tanya);
        $tanya = Tanya::find($id);
        return view('pertanyaan.detail', compact('tanya'));
    }
    public function edit($id)
    {
        // $tanya = DB::table('pertanyaan')->where('id', $id)->first();
        $tanya = Tanya::find($id);
        return view('pertanyaan.ubah', compact('tanya'));
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => "required|unique:pertanyaan",
            'isi' => 'required'
        ]);

        // $query = DB::table('pertanyaan')
        //     ->where('id', $id)
        //     ->update([
        //         'judul' => $request['judul'],
        //         'isi' => $request['isi']
        //     ]);

        $update = Tanya::where('id', $id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Berhasil ubah pertanyaan!');
    }
    public function destroy($id)
    {
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();
        Tanya::destroy($id);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dihapus!');
    }
}
